export class HandlerMsgONSERVER {
  constructor(msg, status, success = false) {
    this.message = msg
    this.status = status
    this.success = success
  }
}
