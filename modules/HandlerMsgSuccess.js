export class HandlerMsgSuccess {
  constructor(data, msg, status, success = true) {
    this.data = data
    this.message = msg
    this.status = status
    this.success = success
  }
}
