import express from "express"
import { apiForVmTotal } from "./api_vm/routes/vm.router"
import { apiVdi } from "./vdi/routes/vdi.route"
import { apiConsole } from "./console/routes/console.route"

export const getHanderAdminSettin = (req, res, next) => {
  if (req.headers.license === "sukum-create") {
    if (req.headers.host_server) {
      const toJsonData = JSON.parse(req.headers.host_server)
      req.host_data = toJsonData
      next()
      // console.log(req.host_data)
    }
  } else {
    res.json({
      message: "You not licens...",
      status: 403
    })
  }
}
export const router = app => {
  const router = express.Router()
  app.use("/test", (req, res, next) => {
    setTimeout(() => {
      res.json({ Hi: "1" })
    }, 10000)
  })
  app.use("/api/v1/xen/server", apiForVmTotal(router))
  app.use("/api/v1/xen/vdi", apiVdi(router))

  app.use("/api/v1/xen/console", apiConsole(router))
}
