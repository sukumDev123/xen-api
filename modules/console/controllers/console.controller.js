import { ConsolePresent } from "../presents/console.present"
import { xapi_return } from "../../../lib/server/xapi"

export const get_all = async (req, res, next) => {
  try {
    const host_require_from_admin_client = req.host_data

    const xapi_control = await xapi_return(host_require_from_admin_client)
    const get_all_console = await new ConsolePresent(xapi_control).get_records()
    res.json(get_all_console)
  } catch (error) {
    next(error)
  }
}
