import * as cs from "../controllers/console.controller"
import { getHanderAdminSettin } from "../../router"

export const apiConsole = router => {
  router.get("/get_all_console", getHanderAdminSettin, cs.get_all)
  return router
}
