import { CreateClientClass } from "../../../lib/xenserver/create_client"

export class ConsolePresent {
  /**
   * @param {CreateClientClass} xapi
   *
   */
  constructor(xapi) {
    this.xapi = xapi.xapi
    this.connect = xapi.connect
    this.getOwnerSession = xapi.getOwnerSession()
  }
  get_records() {
    //   this.xapi
    if (this.connect) {
      return this.xapi._transportCall("console.get_all_records", [
        this.getOwnerSession
      ])
    }
  }
}
