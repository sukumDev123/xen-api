import { getHanderAdminSettin } from "../../router"
import * as vdi from "../controllers/vdi.controller"
export const apiVdi = router => {
  router.post("/get_record_vdi", getHanderAdminSettin, vdi.get_vdi_records)
  return router
}
