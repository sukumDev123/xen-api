import { VBDCLASS } from "../../../lib/xenserver/vdb"
import { HandlerMsgONSERVER } from "../../HandlerMsgOnserver"
import { xapi_return } from "../../../lib/server/xapi"

import { VIF } from "../../../lib/xenserver/vif"

export const get_vdi_records = async (req, res, next) => {
  try {
    console.log(req.host_server)
    if (req.host_data) {
      // const { sr } = req.body
      const host_require_from_admin_client = req.host_data
      const xapi_ = await xapi_return(host_require_from_admin_client)

      const { opa } = req.body

      const vifp = new VIF(xapi_)

      const get_records = await vifp.vif_get_record(opa)
      res.json(get_records)
      // const vdi_class = new VDIClass(xapi_)
      // const vdi_o = await vdi_class.vdi_uuid(uuid)
      // const get_Records = await vdi_class.vdi_get_record(vdi_o)

      // console.log(get_Records)
    } else {
      next(new HandlerMsgONSERVER("Host Not request.", 500))
    }
  } catch (error) {
    console.log(error)
    next(error)
  }
}
