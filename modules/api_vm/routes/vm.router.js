import * as controller from "../controllers/vm.controller"
import { getHanderAdminSettin } from "../../router"

export const apiForVmTotal = router => {
  //   router.get("/get_records", controller.records);

  router.get(
    "/get_records",
    getHanderAdminSettin,
    controller.get_is_not_template
  )
  router.get(
    "/get_template",
    getHanderAdminSettin,
    controller.get_is_a_template
  )
  router.post("/clone", getHanderAdminSettin, controller.clone_vm)
  router.delete("/destory", getHanderAdminSettin, controller.destory_vm)
  router.put("/stop", getHanderAdminSettin, controller.stop_vm)
  router.put("/start", getHanderAdminSettin, controller.vm_start_controller)
  return router
}
