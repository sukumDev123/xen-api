import { VMPreset } from "../presents/vm_control.present"
import { HandlerMsgONSERVER } from "../../HandlerMsgOnserver"
import { HandlerMsgSuccess } from "../../HandlerMsgSuccess"
import { xapi_return } from "../../../lib/server/xapi"

export const records = (req, res, next) => {
  res.json({
    sfa: "sad"
  })
}

export const get_is_a_template = async (req, res, next) => {
  try {
    const host_require_from_admin_client = req.host_data
    const xapi_control = await xapi_return(host_require_from_admin_client)
    const vmP = new VMPreset(xapi_control)
    const get_vm = await vmP.vm_all_records_to_array()
    const vm_is_a_tempalate = get_vm.filter(
      vm => vm.is_a_template === true && vm.is_default_template === false
    )
    res
      .json(
        new HandlerMsgSuccess(
          vm_is_a_tempalate,
          "Success to get result records of vm ",
          200,
          true
        )
      )
      .end()
  } catch (error) {
    next(new HandlerMsgONSERVER(error, 404))
  }
}
export const get_is_not_template = async (req, res, next) => {
  try {
    const host_require_from_admin_client = req.host_data
    const xapi_control = await xapi_return(host_require_from_admin_client)
    const a = new VMPreset(xapi_control)
    const get_vm = await a.vm_all_records_to_array()
    const vm_is_not_template = get_vm.filter(vm => vm.is_a_template === false)
    res
      .json(
        new HandlerMsgSuccess(
          { infor: vm_is_not_template, size: vm_is_not_template.length },
          "Success...",
          200
        )
      )
      .end()
  } catch (error) {
    console.log(error)
    next(new HandlerMsgONSERVER(error, 404))
  }
}
export const destory_vm = async (req, res, next) => {
  try {
    if (req.host_data) {
      // const
      const host_require_from_admin_client = req.host_data
      const xapi_control = await xapi_return(host_require_from_admin_client)
      const vm_preset = new VMPreset(xapi_control)
      const { uuid } = req.body
      const stop_destory = await vm_preset.vm_stop_and_destory(uuid)
      res.json(new HandlerMsgSuccess(true, stop_destory, 200)).end()
    } else {
      next(new HandlerMsgONSERVER("Host data not request.", 500))
    }
  } catch (error) {
    next(new HandlerMsgONSERVER(JSON.stringify(error)), 400)
  }
}

export const clone_vm = async (req, res, next) => {
  try {
    if (req.host_data) {
      const host_require_from_admin_client = req.host_data

      const xapi_control = await xapi_return(host_require_from_admin_client)
      const vm_preset = new VMPreset(xapi_control)

      const clone_vm = await vm_preset.vm_clone_start_and_set_all(
        req.body,
        host_require_from_admin_client
      )
      console.log(clone_vm)
      res
        .json(new HandlerMsgSuccess(clone_vm, "Success to clone vm...", 200))
        .end()
    } else {
      res.json(new HandlerMsgONSERVER("You not input host_server id", 500))
    }
  } catch (error) {
    console.log(error)
    next(new HandlerMsgONSERVER(error, 404))
  }
}

export const stop_vm = async (req, res, next) => {
  try {
    if (req.body.opaqueRef) {
      const host_require_from_admin_client = req.host_data
      const vm_p = new VMPreset(
        await xapi_return(host_require_from_admin_client)
      )
      const opa = req.body.opaqueRef
      const stop = await vm_p.vm_stop(opa)
      res.json(new HandlerMsgSuccess("", stop, 200)).end()
    } else {
      next(new HandlerMsgONSERVER("uuid is not define.", 404))
    }
  } catch (error) {
    // console.log(error);
    next(new HandlerMsgONSERVER(error, 404))
  }
}
export const up_or_down_your_spac = async (req, res, next) => {
  try {
    if (req.body.data && req.body.uuid) {
      const { data, uuid } = req.body

      if (data.infor.cpu && data.infor.memory && data.infor.vdi.size) {
        const host_require_from_admin_client = req.host_data
        const xapiVari = await xapi_return(host_require_from_admin_client)
        const vm_p = new VMPreset(xapiVari)
        const up_down = await vm_p.vm_new_cpu_ram_disk(uuid, data)
        const result_ = up_down
          ? "Update vm spec success."
          : "You cann't Update vm spec success."

        const vm_ref = await vm_p.VM_get_by_uuid(vm_p.getOwnerSession, uuid)
        const callBackData = await vm_p.VM_get_record(
          vm_p.getOwnerSession,
          vm_ref
        )

        res.json(new HandlerMsgSuccess(callBackData, result_, 200))
        res.end()
      }
    }
  } catch (error) {
    next(new HandlerMsgONSERVER(error, 404))
  }
}
export const vm_start_controller = async (req, res, next) => {
  try {
    if (req.host_data && req.body.opaqueRef) {
      const host_require_from_admin_client = req.host_data
      const xapiVari = await xapi_return(host_require_from_admin_client)
      const vm_p = new VMPreset(xapiVari)
      const opa = req.body.opaqueRef
      const start_vm = await vm_p.vm_start_(opa)
      res.json(new HandlerMsgSuccess(true, start_vm, 200)).end()
    }
  } catch (error) {
    // next(new )
    next(new HandlerMsgONSERVER(error, 404))
  }
}
