import { VM } from "../../../lib/xenserver/vm"
import { VDIClass } from "../../../lib/xenserver/vdi"
import { VIF } from "../../../lib/xenserver/vif"
import { VBDCLASS } from "../../../lib/xenserver/vdb"

import { HandlerMsgONSERVER } from "../../HandlerMsgOnserver"

export class VMPreset extends VM {
  constructor(xapi) {
    super(xapi)
    this.xapi_for_use_this_class = xapi
    this.connect_db = false
  }

  check_data_is_not_null(body) {
    const data = {
      clone_from: body.clone_from,
      name_label: body.name_label,
      vif: {
        ip: body.vif.ip,
        getway: body.vif.getway
      },
      vdi: {
        size: body.vdi.size
      },
      infor: {
        cpu: body.infor.cpu,
        memory: body.infor.memory
      }
    }
    if (
      data.clone_from &&
      data.name_label &&
      data.vif.ip &&
      data.vif.getway &&
      data.vdi.size &&
      data.infor.cpu &&
      data.infor.memory
    ) {
      return data
    } else return false
  }

  set_data_beforn_clone(data) {
    return this.check_data_is_not_null(data)
  }
  async set_ip4(vif_o, vif_info) {
    try {
      const xapi_fun = this.xapi_for_use_this_class

      const vif = new VIF(xapi_fun)
      const call = await vif.vif_configure_ip4(
        vif_o,
        vif_info.ip,
        vif_info.getway
      )
      const get_vif = await vif.vif_get_record(vif_o)
      return { message: call, data: get_vif, vif_op: vif_o }
    } catch (error) {
      console.log("ip  : ", error)
      return new Error(error)
    }
  }
  async set_disk_func(vbd_o, vdi_size) {
    try {
      const size_real = parseInt(vdi_size)
      const xapi_fun = this.xapi_for_use_this_class
      let vbd = new VBDCLASS(xapi_fun)
      let vbd0 = await vbd.vbd_get_record(vbd_o[0])
      let vbd1 = await vbd.vbd_get_record(vbd_o[1])
      let vdi
      let vbdThis_
      if (vbd0.userdevice == "0") {
        vdi = vbd0.VDI
        vbdThis_ = vbd_o[0]
      } else if (vbd1.userdevice == "0") {
        vdi = vbd1.VDI
        vbdThis_ = vbd_o[1]
      }
      const vdiclass = new VDIClass(xapi_fun)
      const get_r_vdi = await vdiclass.vdi_resize(vdi, size_real)
      const get_vdi_record = await vdiclass.vdi_get_record(vdi)

      return {
        message: `Set size disk success. ${get_r_vdi}`,
        data: get_vdi_record,
        vdi_o: vdi,
        vbd: vbdThis_
      }
    } catch (error) {
      console.log("Disk error : ", error)
      return new Error(error)
    }
  }
  /**
   *
   * @param {*} uuid // uuid of vm
   * @param {*}  data  data : { infor : {cpu : int , memory : bytes , } vdi : {size : bytes}  }
   */
  async vm_new_cpu_ram_disk(get_op, data) {
    try {
      const get_record = await this.VM_get_record(this.getOwnerSession, get_op)
      if (get_record.power_state === "Halted") {
        this.set_VCPUs_max(this.getOwnerSession, get_op, data.infor.cpu)

        this.set_VCPUs_at_startup(this.getOwnerSession, get_op, data.infor.cpu)
        this.set_memory(this.getOwnerSession, get_op, data.infor.memory)
        const vbd_o = get_record.VBDs
        const vdi_record = await this.set_disk_func(vbd_o, data.vdi.size)
        console.log("Vdi Log : ", vdi_record.message)
        return {
          success: true,
          vdi: vdi_record.data,
          vdi_o: vdi_record.vdi_o,
          vbd: vdi_record.vbd
        }
      } else {
        return { success: false }
      }
    } catch (error) {
      return error
    }
  }

  async delete_vbd_vdi(vbds) {
    try {
      const vbdClass = new VBDCLASS(this.xapi_for_use_this_class)
      const vdiClass = new VDIClass(this.xapi_for_use_this_class)
      const vbd0 = await vbdClass.vbd_get_record(vbds[0])
      const vbd1 = await vbdClass.vbd_get_record(vbds[1])

      if (vbd0.userdevice === "0") {
        vdiClass.vdi_destory(vbd0.VDI)
        return true
      } else if (vbd1.userdevice === "0") {
        vdiClass.vdi_destory(vbd1.VDI)
        return true
      } else {
        return false
      }
    } catch (error) {
      return error
    }
  }
  async vm_stop_and_destory(vm_uuid) {
    try {
      if (this.connect) {
        const vm_o = await this.VM_get_by_uuid(this.getOwnerSession, vm_uuid)
        const get_record = await this.VM_get_record(this.getOwnerSession, vm_o)
        if (get_record.power_state !== "Halted") {
          await this.vm_shutdown(this.getOwnerSession, vm_o)
        }
        const vbds = get_record.VBDs
        this.delete_vbd_vdi(vbds)
        this.vm_destroy(vm_o)
        return `Delete your vm clone success ${+new Date()}`
      }
      return `not delete.`
    } catch (error) {
      return error
    }
  }

  async vm_clone_start_and_set_all(body) {
    try {
      if (this.connect) {
        const data = this.set_data_beforn_clone(body)
        if (data) {
          // console.log(data.vdi.size);
          const vmClone = await this.VM_get_by_uuid(
            this.getOwnerSession,
            data.clone_from
          )
          const start_to_clone = await this.VM_clone(
            this.getOwnerSession,
            vmClone,
            data.name_label
          ) // clone vm
          console.log("Log new clone start : ", start_to_clone)

          const get_record_vm = await this.VM_get_record(
            this.getOwnerSession,
            start_to_clone
          ) // get vm record
          // console.log("Save Vm on database LOG : ", save_data_on_database)

          this.VM_set_is_a_template(start_to_clone, false) // template = false

          const status_set = await this.vm_new_cpu_ram_disk(
            start_to_clone,
            data
          ) // set cpu memory disk

          if (status_set.success) {
            const vif_o = get_record_vm.VIFs[0]
            const data_ip = await this.set_ip4(vif_o, data.vif)
            console.log(`set ip: `, data_ip)
            this.vm_start(this.getOwnerSession, start_to_clone, false, true)
            console.log("start vm : ")

            const get_data_record_show = await this.VM_get_record(
              this.getOwnerSession,
              start_to_clone
            )

            const dataTotal = {
              vmO: start_to_clone,
              vm: get_data_record_show,
              vifO: data_ip.vif_op,
              vif: data_ip.data,
              vdi: status_set.vdi,
              vdiO: status_set.vdi_o,
              vbd: {
                vm_o: start_to_clone,
                vdi_o: status_set.vdi_o,
                vbd_o: status_set.vbd
              }
            }
            return dataTotal
          } else {
            return new Error(
              "Set Cpu , Memory , Disk have a Problem or Vm Runing."
            )
          }
        } else {
          return Promise.reject(
            new HandlerMsgONSERVER("Some filed is empty.", 400)
          )
        }
      } else {
        return Promise.reject(new HandlerMsgONSERVER("Connect false.", 500))
      }
    } catch (error) {
      console.log(error)
      return new Error(error)
    }
  }

  async vm_stop(vm_o) {
    try {
      await this.vm_shutdown(this.getOwnerSession, vm_o)
      return `Stop vm time ${+new Date()}`
    } catch (error) {
      return error
    }
  }
  async vm_start_(vm_o) {
    try {
      // await this.vm_start(vm_o);
      await this.vm_start(this.getOwnerSession, vm_o, false, true)
      return `Started vm time to ${+new Date()}`
    } catch (error) {
      return error
    }
  }
  async vm_all_records_to_array() {
    try {
      const all = await this.VM_get_all()
      let records = all.map(
        async data => await this.VM_get_record(this.getOwnerSession, data)
      )
      return Promise.all(records)
    } catch (error) {
      return new Error(error)
    }
  }
}
