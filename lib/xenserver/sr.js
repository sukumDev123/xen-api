import { CreateClientClass } from "./create_client"

export class SRCALSS {
  /**
   *
   * @param {CreateClientClass} xapi
   */
  constructor(xapi) {
    this.xapi = xapi.xapi
    this.connect = xapi.connect
    this.getOwnerSession = xapi.getOwnerSession()
  }
  get_all_records() {
    return this.xapi._sessionCall("SR.get_all_records")
  }
  get_records(srO) {
    if (this.connect) {
      return this.xapi._transportCall("SR.get_record", [
        this.getOwnerSession,
        srO
      ])
    }
  }
  sr_destory(srO) {
    if (this.connect) {
      return this.xapi._transportCall("SR.destroy", [this.getOwnerSession, srO])
    }
  }
}
