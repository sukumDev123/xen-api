import { CreateClientClass } from "./create_client";

export class Host {
  /**
   *
   * @param {CreateClientClass} xapi
   */
  constructor(xapi) {
    this.xapi = xapi.xapi;
    this.connect = xapi.connect;
    this.getOwnerSession = xapi.getOwnerSession();
  }
  host_get_all() {
    if (this.connect) {
      return this.xapi._transportCall("host.get_all_records", [
        this.getOwnerSession
      ]);
    }
  }
}
