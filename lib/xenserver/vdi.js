import { CreateClientClass } from "./create_client"

export class VDIClass {
  /**
   *
   * @param {CreateClientClass} create_xapi
   */
  constructor(create_xapi) {
    this.xapi = create_xapi.xapi
    this.connect = create_xapi.connect
    this.getOwnerSession = create_xapi.getOwnerSession()
  }
  vdi_get_record(OpaqueRefNew_vdi) {
    if (this.connect) {
      return this.xapi._transportCall("VDI.get_record", [
        this.getOwnerSession,
        OpaqueRefNew_vdi
      ])
    }
    return new Error("Isn't connected ")
  }
  vdi_uuid(uuid) {
    if (this.connect) {
      return this.xapi._transportCall("VDI.get_by_uuid", [
        this.getOwnerSession,
        uuid
      ])
    }
  }
  vdi_destory(OpaqueRefNew_vdi) {
    if (this.connect) {
      return this.xapi._transportCall("VDI.destroy", [
        this.getOwnerSession,
        OpaqueRefNew_vdi
      ])
    }
    return new Error("Isn't connected ")
  }

  /**
   * This method for set disk of storage
   * @param {*} OpaqueRefNew_vdi
   * @param {*} sizeisNeed
   */
  vdi_resize(OpaqueRefNew_vdi, sizeisNeed) {
    try {
      if (this.connect) {
        return this.xapi._transportCall("VDI.resize", [
          this.getOwnerSession,
          OpaqueRefNew_vdi,
          sizeisNeed * 1073741824
        ])
      }
      return new Error("Isn't connected ")
    } catch (error) {
      console.log(error)
      return error
    }
  }
}
