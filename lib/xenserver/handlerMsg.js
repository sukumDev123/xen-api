export class HandlerMsg {
  constructor(msg, data, success = false) {
    this.msg = msg
    this.data = data
    this.success = success
  }
}
