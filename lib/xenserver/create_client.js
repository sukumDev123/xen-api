import { createClient } from "xen-api/dist"

/**
 * @returns {CreateClientClass}
 */
export class CreateClientClass {
  constructor(url, auth) {
    this.xapi = createClient({
      url: url,
      allowUnauthorized: false,
      auth: {
        user: auth.user,
        password: auth.password
      },
      readOnly: false
    })
    this.connect = false
  }
  async connect_vm() {
    await this.xapi.connect()
    this.connect = true
  }
  getOwnerSession() {
    if (this.connect) {
      return this.xapi.sessionId
    }
    return new Error("Is'n connected...")
  }
}
