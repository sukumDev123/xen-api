import { CreateClientClass } from "./create_client"

export class VBDCLASS {
  /**
   *
   * @param {CreateClientClass} create_xapi
   */
  constructor(create_xapi) {
    this.xapi = create_xapi.xapi
    this.connect = create_xapi.connect
    this.getSessionOwner = create_xapi.getOwnerSession()
  }
  vbd_unplug(OpaqueRefNew_of_vbd) {
    if (this.connect) {
      return this.xapi._transportCall("VBD.unplug", [
        this.getSessionOwner,
        OpaqueRefNew_of_vbd
      ])
    }
  }
  vbd_get_record(OpaqueRefNew_of_vbd) {
    if (this.connect) {
      return this.xapi._transportCall("VBD.get_record", [
        this.getSessionOwner,
        OpaqueRefNew_of_vbd
      ])
    }
  }
  vbd_destory(OpaqueRefNew_vbd) {
    if (this.connect) {
      return this.xapi._transportCall("VBD.destroy", [
        this.getOwnerSession,
        OpaqueRefNew_vbd
      ])
    }
    return new Error("Isn't connected ")
  }
}
