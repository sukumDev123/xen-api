import { HandlerMsg } from "./handlerMsg"

export class VIF {
  /**
   *
   * @param {CreateClientClass} create_client
   */
  constructor(create_client) {
    this.xapi = create_client.xapi
    this.connect = create_client.connect
    this.getOwnerSession = create_client.getOwnerSession()
  }

  vif_Get_uuid(uuid) {
    if (this.connect) {
      if (this.getOwnerSession) {
        return this.xapi._transportCall("VIF.get_by_uuid", [
          this.getOwnerSession,
          uuid
        ])
      }
      return new Error("Session is empty...")
    }
    return new Error("Isn't connected...")
  }

  /**
   * this get record of vif.
   * @param {*} vif
   */
  vif_get_record(vif) {
    if (this.connect) {
      if (this.getOwnerSession) {
        return this.xapi._transportCall("VIF.get_record", [
          this.getOwnerSession,
          vif
        ])
      }
      return new Error("Session is empty...")
    }
    return new Error("Isn't connected...")
  }

  /**
   * This function for set ip
   * @param {*} vif
   * @param {*} mode
   * @param {*} ip
   * @param {*} getway
   * @returns {HandlerMsg}
   */
  vif_configure_ip4(vif, ip, getway) {
    if (this.connect) {
      if (this.getOwnerSession) {
        return this.xapi._transportCall("VIF.configure_ipv4", [
          this.getOwnerSession,
          vif,
          "Static",
          ip,
          getway
        ])
      } else {
        return new Error("Isn't connected...")
      }
    }

    // return new Error("Is'n not connect");
  }
}
