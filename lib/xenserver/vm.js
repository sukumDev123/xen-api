import { CreateClientClass } from "./create_client"

export class VM {
  /**
   *
   * @param {CreateClientClass} client_create
   */
  constructor(client_create) {
    this.xapi = client_create.xapi
    this.connect = client_create.connect
    this.getOwnerSession = client_create.getOwnerSession()
  }

  // return promise
  VM_get_all() {
    if (this.connect) {
      const idSession = this.getOwnerSession
      return this.xapi._transportCall("VM.get_all", [idSession])
    }
    return new Error("Is'n connected")
  }
  // return promise
  VM_get_record(id_session, recodes_is) {
    if (this.connect) {
      return this.xapi._transportCall("VM.get_record", [id_session, recodes_is])
    }
    return new Error("Is'n connected")
  }

  /**
   * This method if you know uuid for vm. You can use this method for find vm.OpaqueRefNew.
   * @param {*} id_session
   * @param {*} uuid
   */
  VM_get_by_uuid(id_session, uuid) {
    if (this.connect) {
      return this.xapi._transportCall("VM.get_by_uuid", [id_session, uuid])
    }
    return new Error("Is'n connected")
  }

  /**
   * Clone vm form template . After clone you need to set_template is false.
   * @param {*} id_session
   * @param {*} cloned_form is vm exists want to get clone
   * @param {*} name_new
   */
  VM_clone(id_session, cloned_form, name_new) {
    if (this.connect) {
      return this.xapi._transportCall("VM.clone", [
        id_session,
        cloned_form,
        name_new
      ])
    }
    return new Error("Is'n connected")
  }

  /**
   * set rams
   * @param {*} id_session
   * @param {*} vm //OpaqueRefNew of vm.
   * @param {*} bytes_memory  // Set memory you want to use .
   */
  set_memory(id_session, vm, bytes_memory) {
    if (this.connect) {
      return this.xapi._transportCall("VM.set_memory", [
        id_session,
        vm,
        parseInt(bytes_memory) * 1073741824
      ])
    }
    return new Error("Is'n connected")
  }

  /**
   * This method start vm .
   * @param {*} id_session // session owner
   * @param {*} OpaqueRefNew  // OpaqueRefNew of vm your want to start
   * @param {*} start_paused // you want to paused
   * @param {*} force  // set true
   */
  vm_start(id_session, OpaqueRefNew, start_paused = false, force = true) {
    if (this.connect) {
      return this.xapi._transportCall("VM.start", [
        id_session,
        OpaqueRefNew,
        start_paused,
        force
      ])
    }
    return new Error("Is'n connected")
  }
  vm_shutdown(id_session, OpaqueRefNew) {
    if (this.connect) {
      return this.xapi._transportCall("VM.shutdown", [id_session, OpaqueRefNew])
    }
    return new Error("Is'n connected")
  }
  // void
  set_VCPUs_max(id_session, OpaqueRefNew, core_cpu) {
    if (this.connect) {
      return this.xapi._transportCall("VM.set_VCPUs_max", [
        id_session,
        OpaqueRefNew,
        core_cpu
      ])
    }
    return new Error("Is'n connected")
  }
  // void
  set_VCPUs_at_startup(id_session, OpaqueRefNew, core_cpu) {
    if (this.connect) {
      return this.xapi._transportCall("VM.set_VCPUs_at_startup", [
        id_session,
        OpaqueRefNew,
        core_cpu
      ])
    }
    return new Error("Is'n connected")
  }

  // void
  VM_set_is_a_template(OpaqueRefNew, is_template) {
    if (this.connect) {
      return this.xapi._transportCall("VM.set_is_a_template", [
        this.getOwnerSession,
        OpaqueRefNew,
        is_template
      ])
    }
  }

  /**
   * This method to destroy vm.
   * @param {*} OpaqueRefNew
   */
  vm_destroy(OpaqueRefNew) {
    if (this.connect) {
      return this.xapi._transportCall("VM.destroy", [
        this.getOwnerSession,
        OpaqueRefNew
      ])
    }
  }
  VM_get_blobs(id_session, OpaqueRefNew) {
    if (this.connect) {
      return this.xapi._transportCall("VM.get_blobs", [
        id_session,
        OpaqueRefNew
      ])
    }
  }
}
