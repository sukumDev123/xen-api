import path from "path"
import fs from "fs"
import ursa from "ursa"

export class Rsa {
  constructor(data) {
    this.data = data
  }
  create_pri() {
    const privateKey = ursa.createPrivateKey(
      fs.readFileSync(path.resolve("./key/priKey.pem"))
    )
    return privateKey
  }
  create_pub() {
    const publicKey = ursa.createPublicKey(
      fs.readFileSync(path.resolve("./key/pubKey.pem"))
    )
    return publicKey
  }
  encrypt_data() {
    return this.create_pub().encrypt(this.data, "utf8", "base64")
  }
  decrypt_data() {
    return this.create_pri().decrypt(this.data, "base64", "utf8")
  }
}
