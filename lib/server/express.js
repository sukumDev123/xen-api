import express from "express"
import { apiForVmTotal } from "../../modules/api_vm/routes/vm.router"
import bodyParser from "body-parser"
import morgan from "morgan"

import { router } from "../../modules/router"
function headerSet(req, res, next) {
  res.setHeader("Access-Control-Allow-Origin", "*")
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, OPTIONS, PUT, PATCH, DELETE"
  )
  res.setHeader("Access-Control-Allow-Credentials", true)
  res.setHeader(
    "Access-Control-Allow-Headers",
    "X-Requested-With,content-type,Authorization,license"
  )
  next()
}
const checkCert = (req, res, next) => {
  const cert = req.connection.getPeerCertificate()
  if (req.client.authorized) {
    next()
  } else if (cert.subject) {
    res.json({
      message: `Sorry ${cert.subject.CN}, certificates from ${
        cert.issuer.CN
      } are not welcome here.`,
      status: 403
    })
    // res.end()
  } else {
    res.json({
      message: `Sorry, but you need to provide a client certificate to continue.`,
      status: 401
    })
    // res.end()
  }
}
const middleWare = app => {
  app.use(checkCert)
  app.use(
    bodyParser.urlencoded({
      extended: false
    })
  )
  app.use(bodyParser.json())
  app.use(headerSet)
  // passport.initialize();
}
const setTest = (req, res, next) => {
  res.json({
    hello: "world"
  })
}

export const startHttp = () => {
  const app = express()
  if (process.env.NODE_ENV === "development") {
    app.use(morgan("dev"))
  }
  middleWare(app)
  router(app)
  return app
}
