import { CreateClientClass } from "../xenserver/create_client"
import { Rsa } from "./Rsa"

export const xapi_return = async data_host => {
  try {
    if (data_host) {
      const host_ip = `http://${data_host.url}`
      const xapi = new CreateClientClass(host_ip, data_host.auth)
      if (!xapi.connect) {
        await xapi.connect_vm()
      }

      return xapi
    } else {
      return {
        meesage: "conenct find this host.",
        status: 300
      }
    }
  } catch (error) {
    console.log(error)
    // return new Error(error)
  }
}
