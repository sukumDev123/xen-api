FROM node:8.9-alpine
# ENV NODE_ENV production
RUN apk add --update \
    python \
    g++ \ 
    gcc \
    make \
    && rm -rf /var/cache/apk/*
RUN npm install -g node-gyp yarn pm2
RUN mkdir /node 
WORKDIR /node
COPY . /node 
RUN yarn
EXPOSE 3000

# COPY . /node/start
# EXPOSE 3000
# CMD npm start