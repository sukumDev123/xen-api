import http from "http"
import { startHttp } from "./lib/server/express"
import https from "https"
import fs from "fs"
const options = {
  key: fs.readFileSync("./cert/server_key.pem"),
  cert: fs.readFileSync("./cert/server_cert.pem"),
  ca: [fs.readFileSync("./cert/server_cert.pem")],
  requestCert: true,
  rejectUnauthorized: false
}
const startServer = () => {
  http.createServer(startHttp()).listen(3000, () => {
    console.log(
      "Service start api listen : 3000",
      "Env : ",
      process.env.NODE_ENV
    )
  })
  const httpsPort = 3001
  https.createServer(options, startHttp()).listen(httpsPort, () => {
    console.log("Service start https listen port  : ", httpsPort)
  })
}

startServer()
